package ru.t1.tbobkov.tm.repository;

import ru.t1.tbobkov.tm.api.repository.ICommandRepository;
import ru.t1.tbobkov.tm.constant.ArgumentConstant;
import ru.t1.tbobkov.tm.constant.CommandConstant;
import ru.t1.tbobkov.tm.model.Command;

public final class CommandRepository implements ICommandRepository {


    private static final Command TASK_REMOVE_BY_ID = new Command(CommandConstant.TASK_REMOVE_BY_ID, null, "find task by id and remove it from storage");

    private static final Command TASK_REMOVE_BY_INDEX = new Command(CommandConstant.TASK_REMOVE_BY_INDEX, null, "find task by index and remove it from storage");

    private static final Command TASK_UPDATE_BY_ID = new Command(CommandConstant.TASK_UPDATE_BY_ID, null, "find task by id and update its data");

    private static final Command TASK_UPDATE_BY_INDEX = new Command(CommandConstant.TASK_UPDATE_BY_INDEX, null, "find task by index and update its data");

    private static final Command TASK_SHOW_BY_ID = new Command(CommandConstant.TASK_SHOW_BY_ID, null, "find task by id and show its data");

    private static final Command TASK_SHOW_BY_INDEX = new Command(CommandConstant.TASK_SHOW_BY_INDEX, null, "find task by index and show its data");

    private static final Command PROJECT_REMOVE_BY_ID = new Command(CommandConstant.PROJECT_REMOVE_BY_ID, null, "find project by id and remove it from storage");

    private static final Command PROJECT_REMOVE_BY_INDEX = new Command(CommandConstant.PROJECT_REMOVE_BY_INDEX, null, "find project by index and remove it from storage");

    private static final Command PROJECT_UPDATE_BY_ID = new Command(CommandConstant.PROJECT_UPDATE_BY_ID, null, "find project by id and update its data");

    private static final Command PROJECT_UPDATE_BY_INDEX = new Command(CommandConstant.PROJECT_UPDATE_BY_INDEX, null, "find project by index and update its data");

    private static final Command PROJECT_SHOW_BY_ID = new Command(CommandConstant.PROJECT_SHOW_BY_ID, null, "find project by id and show its data");

    private static final Command PROJECT_SHOW_BY_INDEX = new Command(CommandConstant.PROJECT_SHOW_BY_INDEX, null, "find project by index and show its data");

    private static final Command HELP = new Command(CommandConstant.HELP, ArgumentConstant.HELP, "show this list of commands");

    private static final Command VERSION = new Command(CommandConstant.VERSION, ArgumentConstant.VERSION, "show version info");

    private static final Command ABOUT = new Command(CommandConstant.ABOUT, ArgumentConstant.ABOUT, "show developer info");

    private static final Command INFO = new Command(CommandConstant.INFO, ArgumentConstant.INFO, "show system info");

    private static final Command EXIT = new Command(CommandConstant.EXIT, null, "close application");

    private static final Command PROJECT_LIST = new Command(CommandConstant.PROJECT_LIST, null, "show project list");

    private static final Command PROJECT_CREATE = new Command(CommandConstant.PROJECT_CREATE, null, "create new project");

    private static final Command PROJECT_CLEAR = new Command(CommandConstant.PROJECT_CLEAR, null, "clear projects");

    private static final Command TASK_LIST = new Command(CommandConstant.TASK_LIST, null, "show task list");

    private static final Command TASK_CREATE = new Command(CommandConstant.TASK_CREATE, null, "create new task");

    private static final Command TASK_CLEAR = new Command(CommandConstant.TASK_CLEAR, null, "clear tasks");

    private static final Command PROJECT_CHANGE_STATUS_BY_INDEX = new Command(CommandConstant.PROJECT_CHANGE_STATUS_BY_INDEX, null, "find project by index and change its status");

    private static final Command PROJECT_CHANGE_STATUS_BY_ID = new Command(CommandConstant.PROJECT_CHANGE_STATUS_BY_ID, null, "find project by id and change its status");

    private static final Command PROJECT_START_BY_INDEX = new Command(CommandConstant.PROJECT_START_BY_INDEX, null, "find project by index and change its status to 'In Progress'");

    private static final Command PROJECT_START_BY_ID = new Command(CommandConstant.PROJECT_START_BY_ID, null, "find project by id and change its status to 'In Progress'");

    private static final Command PROJECT_COMPLETE_BY_INDEX = new Command(CommandConstant.PROJECT_COMPLETE_BY_INDEX, null, "find project by index and change its status to 'Completed'");

    private static final Command PROJECT_COMPLETE_BY_ID = new Command(CommandConstant.PROJECT_COMPLETE_BY_ID, null, "find project by id and change its status to 'Completed'");

    private static final Command TASK_CHANGE_STATUS_BY_INDEX = new Command(CommandConstant.TASK_CHANGE_STATUS_BY_INDEX, null, "find task by index and change its status");

    private static final Command TASK_CHANGE_STATUS_BY_ID = new Command(CommandConstant.TASK_CHANGE_STATUS_BY_ID, null, "find task by id and change its status");

    private static final Command TASK_START_BY_INDEX = new Command(CommandConstant.TASK_START_BY_INDEX, null, "find task by index and change its status to 'In Progress'");

    private static final Command TASK_START_BY_ID = new Command(CommandConstant.TASK_START_BY_ID, null, "find task by id and change its status to 'In Progress'");

    private static final Command TASK_COMPLETE_BY_INDEX = new Command(CommandConstant.TASK_COMPLETE_BY_INDEX, null, "find task by index and change its status to 'Completed'");

    private static final Command TASK_COMPLETE_BY_ID = new Command(CommandConstant.TASK_COMPLETE_BY_ID, null, "find task by id and change its status to 'Completed'");

    private static final Command[] COMMANDS = new Command[]{
            HELP, VERSION, ABOUT, INFO,

            PROJECT_LIST, PROJECT_CREATE, PROJECT_CLEAR,
            PROJECT_SHOW_BY_INDEX, PROJECT_SHOW_BY_ID,
            PROJECT_UPDATE_BY_INDEX, PROJECT_UPDATE_BY_ID,
            PROJECT_REMOVE_BY_INDEX, PROJECT_REMOVE_BY_ID,
            PROJECT_CHANGE_STATUS_BY_INDEX, PROJECT_CHANGE_STATUS_BY_ID,
            PROJECT_START_BY_INDEX, PROJECT_START_BY_ID,
            PROJECT_COMPLETE_BY_INDEX, PROJECT_COMPLETE_BY_ID,

            TASK_LIST, TASK_CREATE, TASK_CLEAR,
            TASK_SHOW_BY_INDEX, TASK_SHOW_BY_ID,
            TASK_UPDATE_BY_INDEX, TASK_UPDATE_BY_ID,
            TASK_REMOVE_BY_INDEX, TASK_REMOVE_BY_ID,
            TASK_CHANGE_STATUS_BY_INDEX, TASK_CHANGE_STATUS_BY_ID,
            TASK_START_BY_INDEX, TASK_START_BY_ID,
            TASK_COMPLETE_BY_INDEX, TASK_COMPLETE_BY_ID,

            EXIT
    };

    public Command[] getCommands() {
        return COMMANDS;
    }

}
